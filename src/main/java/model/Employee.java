/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author Nopparuth
 */
public class Employee {
    private int id;
    private String firstName;
    private String lastName;
    private String tel;
    private int idCard;
    private double salary;
    private String bankAccount;
    private Timestamp startDate;
    private int empid;

    public Employee(int id, String firstName, String lastName, String tel, int idCard, double salary, String bankAccount, Timestamp startDate, int empid) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.tel = tel;
        this.idCard = idCard;
        this.salary = salary;
        this.bankAccount = bankAccount;
        this.startDate = startDate;
        this.empid = empid;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getIdCard() {
        return idCard;
    }

    public void setIdCard(int idCard) {
        this.idCard = idCard;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public int getEmpid() {
        return empid;
    }

    public void setEmpid(int empid) {
        this.empid = empid;
    }

    @Override
    public String toString() {
        return "Employee{" + "id= 00" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", tel=" + tel + ", idCard=" + idCard + ", salary=" + salary + ", bankAccount=" + bankAccount + ", startDate=" + startDate + ", empid=" + empid + '}';
    }
    
}
