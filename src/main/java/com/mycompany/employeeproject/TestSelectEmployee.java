/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.employeeproject;

import database.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Employee;

/**
 *
 * @author Nopparuth
 */
public class TestSelectEmployee {
    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT Employee_ID, FirstName, LastName, Tel, ID_Card, Salary, BankAccount, start_Date, Emp_Type_ID FROM Employee";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
               int id = result.getInt("Employee_ID");
               String FirstName = result.getString("FirstName");
               String LastName = result.getString("LastName");
               String Tel = result.getString("Tel");
               int ID_Card = result.getInt("ID_Card");               
               double Salary = result.getDouble("Salary");
               String BankAccount = result.getString("BankAccount");
//               Timestamp start_Date = result.getTimestamp("start_Date");
               int empID = result.getInt("Emp_Type_ID");
               Employee employee = new Employee(id, FirstName, LastName, Tel, ID_Card, Salary, BankAccount, null, empID);
               System.out.println(employee);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }
}
