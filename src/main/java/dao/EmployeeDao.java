/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.mycompany.employeeproject.TestSelectEmployee;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Employee;

/**
 *
 * @author Dell
 */
public class EmployeeDao implements DaoInterface<Employee>{
    @Override
    public int add(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Employee (FirstName, LastName, Tel, ID_Card, Salary, BankAccount, start_Date, Emp_Type_ID) VALUES ?, ?, ?, ?, ?, ?, ?, ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getEmpid());
            stmt.setString(2, object.getFirstName());
            stmt.setString(3, object.getLastName());
            stmt.setString(4, object.getTel());
            stmt.setInt(5, object.getIdCard());
            stmt.setDouble(6, object.getSalary());
            stmt.setString(7, object.getBankAccount());
            stmt.setInt(8, object.getEmpid());
            int row =stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if(result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Employee> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT Employee_ID, FirstName, LastName, Tel, ID_Card, Salary, BankAccount, start_Date, Emp_Type_ID FROM Employee";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()) {
                int id = result.getInt("Employee_ID");
                String firstName = result.getString("FirstName");
                String lastName = result.getString("LastName");
                String tel = result.getString("Tel");
                int idCard = result.getInt("ID_Card");
                double salary = result.getDouble("Salary");
                String bankAccount = result.getString("BankAccount");
                int idEmpType = result.getInt("Emp_Type_ID");
                Employee employee = new Employee(id, firstName, lastName, tel, idCard, salary, bankAccount, null, idEmpType);
                list.add(employee);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public Employee get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT Employee_ID, FirstName, LastName, Tel, ID_Card, Salary, BankAccount, start_Date, Emp_Type_ID FROM Employee WHERE Employee_ID =" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if(result.next()) {
                int eid = result.getInt("Employee_ID");
                String firstName = result.getString("FirstName");
                String lastName = result.getString("LastName");
                String tel = result.getString("Tel");
                int idCard = result.getInt("ID_Card");
                double salary = result.getDouble("Salary");
                String bankAccount = result.getString("BankAccount");
                int idEmpType = result.getInt("Emp_Type_ID");
                Employee employee = new Employee(eid, firstName, lastName, tel, idCard, salary, bankAccount, null,idEmpType);
                return employee;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "DELETE FROM Employee WHERE Employee_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row =stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE Employee SET FirstName = ?, LastName = ?, Tel = ?, ID_Card = ?, Salary = ?, BankAccount = ?, Start_Date = ?, Emp_Type_ID = ? WHERE  Employee_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getFirstName());
            stmt.setString(2, object.getLastName());
            stmt.setString(3, object.getTel());
            stmt.setInt(4, object.getIdCard());
            stmt.setDouble(5, object.getSalary());
            stmt.setString(6, object.getBankAccount());
            stmt.setInt(7, object.getEmpid());
            row =stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }
    public static void main(String[] args) {
        EmployeeDao dao = new EmployeeDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Employee(1, "Davika", "Hone", "0127777777", 123456, 10000, "1234564444", null, 1));
        System.out.println("id: " + id);
        Employee lastEmployee = dao.get(id);
        System.out.println("last Employee: " + lastEmployee);
        lastEmployee.setEmpid(4);
        int row = dao.update(lastEmployee);
        Employee updateEmployee = dao.get(id);
        System.out.println("update Employee: " + updateEmployee);
        dao.delete(id);
        Employee deleteEmployee = dao.get(id);
        System.out.println("delete Employee: " + deleteEmployee);
    }
}
